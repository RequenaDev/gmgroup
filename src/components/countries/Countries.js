import React, { useState } from 'react';
import styled from 'styled-components';
import BtnContact from 'components/BtnContact';
import argImage from 'assets/argentina.png';
import uruImage from 'assets/uruguay.png';
import paraImage from 'assets/paraguay.png';
import chiImage from 'assets/chile.png';
import braImage from 'assets/brasil.png';



const CountriesStyled = styled.div`
	width: 100%;
	height: auto;
	margin: auto;
	margin-top: 3em;
	background: var(--bg-country);
	clip-path: polygon(0 0, 100% 22%, 100% 100%, 0 78%);
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	padding: 6em 2em;
	box-sizing: border-box;
	position: relative;
	overflow: hidden;
	h2 {
		display: block;
		text-align: center;
		font-size: 40px;
		margin-top: 1em;
		background-image: var(--countryTitleBg);
  		-webkit-background-clip: text;
  		-webkit-text-fill-color: transparent;
	}
	p {
		padding: 0;
		margin: 0;
		color: #48cde3;
	}
	.flags {
		display: flex;
		column-gap: .6em;
		padding: 0 .4em;
		z-index: 50;
		img {
			width: 60px;
			height: 60px;
		}
	}
	.btn-contact {
		animation: salt ease 4s infinite;
	}
	.bg-country {
		position: absolute;
		opacity: .08;
		font-size: 8rem;
		letter-spacing: .2em;
		text-transform: uppercase;
		overflow: hidden;
		color: red;
	}
	@media screen and (max-width: 500px) {
		clip-path: polygon(0 1%, 100% 0, 100% 100%, 0 100%);
		height: 90vh;
	}
	@keyframes salt {
		0% {
			transform: translateY(0px);
		}
		10% {
			transform: translateY(-20px);
		}
		20% {
			transform: translateY(0px);
		}
		100% {
			transform: translateY(0px);
		}
	}
`;

function Countries({ handleToggleModal, language }) {
	const [background, setBackground] = useState('');

	const handleBackground = (e) => {
		setBackground(e.target.alt);
		setTimeout(() => {
			setBackground('');
		}, 8000);
	};

	return (
		<CountriesStyled>
			<h2>
			{
				language === 'ES'
				? '+ de 300 empresas en la región eligen nuestros servicios.'
				: '+ More than 300 companies in the region choose our services.'
			}
			</h2>
			<p>{
				language === 'ES'
				? 'Nos encontramos en'
				: 'We are in'
			}
			</p>
			<div className="flags">
				<img
				src={argImage}
				alt="Argentina"
				onMouseMove={handleBackground}
				/>
				<img
				src={uruImage}
				alt="Uruguay"
				onMouseMove={handleBackground}
				/>
				<img
				src={paraImage}
				alt="Paraguay"
				onMouseMove={handleBackground}
				/>
				<img
				src={chiImage}
				alt="Chile"
				onMouseMove={handleBackground}
				/>
				<img
				src={braImage}
				alt="Brasil"
				onMouseMove={handleBackground}
				/>
			</div>
			<BtnContact
			text={language === 'ES' ? 'Contactanos' : 'Contact us'}
			clases="btn-contact"
			handleToggleModal={handleToggleModal}
			/>
			<h2 className="bg-country">{background}</h2>
		</CountriesStyled>
	)
}

export default Countries