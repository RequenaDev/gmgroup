import React from 'react';
import styled from 'styled-components';
import CompanyCard from './CompanyCard';
import calendarIcon from 'assets/calendar.png';
import martilloIcon from 'assets/martillo.png';
import callWomanIcon from 'assets/callWoman.png';
import DoubleArrowRoundedIcon from '@material-ui/icons/DoubleArrowRounded';

const EmpresaStyled = styled.div`
	width: 100%;
	height: auto;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	position: relative;
	h2 {
		background-image: linear-gradient(15deg, #13547a 0%, #80d0c7 100%);
  		-webkit-background-clip: text;
  		-webkit-text-fill-color: transparent;
  		font-size: 60px;
  		display: block;
  		text-align: center;
		padding-top: 1.2em;
	}
	p {
		display: block;
		text-align: center;
		font-size: 1.6rem;
	}
	.p-1 {color: #70bebc;}
	.p-2 {color: #276a88;}
	.billing-grid {
		width: 86%;
		height: auto;
		margin: auto;
		display: grid;
		grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
		gap: 1em;
		padding-top: 3em;
		justify-content: center;
		align-items: center;
	}
	@media screen and (min-width: 1000px) {
		.billing-grid {
			width: 1000px;
		}
	}
	@media screen and (max-width: 375px) {
		.billing-grid {
			padding: 0 0;
			margin: auto 0;
			width: 100%;
		}
	}
`;

function Empresa({ language }) {
	return (
		<EmpresaStyled id="nosotros">
			<h2>GM GROUP</h2>
			<p className="p-1">
			{
				language === 'ES'
				? "Somos expertos en la elaboración de estrategias de cobro de deudas"
				: "We are experts in the development of debt collection strategies"
			}
			</p>
			<p className="p-2">
			{
				language === 'ES'
				? "Sabemos cómo recuperar tu dinero."
				: "We know how to get your money back."
			}
			</p>
			<div className="billing-grid">
				<CompanyCard
				imgUrl={calendarIcon}
				text={
					language === 'ES'
					? "cobranza preventiva"
					: "preventive collection"
					}
				backText={
					language === 'ES'
					? "La cobranza preventiva consiste en la primera gestión de cobro, cuando las facturas y obligaciones de pago están próximas a su vencimiento, con la que se busca lograr el pago de los clientes respetando la fecha de pago establecida."
					: "Preventive collection consists of the first collection management, when invoices and payment obligations are close to maturity, which seeks to achieve payment for customers respecting the established payment date."
					}
				/>
				<CompanyCard
				imgUrl={callWomanIcon}
				text={
					language === 'ES'
					? "cobranza tardia o extrajudicial"
					: "late or extrajudicial collection"
				}
				backText={
					language === 'ES'
					?"En GM GROUP recuperamos su cartera vencida elaborando estrategias en cobranza a su medida, buscando crear vínculos a largo plazo, y a través de procesos de cobranza que buscan recuperar su dinero en el menor tiempo posible."
					:"In GM GROUP we recover your overdue portfolio by developing strategies in collection to suit you, seeking to create long-term links, and through collection processes that seek to recover your money in the shortest possible time."
					}
				/>
				<CompanyCard
				imgUrl={martilloIcon}
				text={
					language === 'ES'
					? "cobranza tardia judicial"
					: "late judicial collection"
				}
				backText={
					language === 'ES'
					?"Una vez agotadas las oportunidades del acuerdo extrajudicial, puede procederse al inicio de acciones judiciales para el cobro de una deuda. En GM GROUP brindamos el asesoramiento oportuno frente a la toma de decisiones legales y el análisis de la información de su cliente para lograr la mejor estrategia en el proceso judicial."
					:"Once the opportunities for out-of-court settlement have been exhausted, legal action can be initiated for the collection of a debt. At GM GROUP we provide timely advice on legal decision-making and analysis of your client's information to achieve the best strategy in the judicial process."
					}
				/>
			</div>
			<a
			href="#nosotros"
			>
			<DoubleArrowRoundedIcon
			style={{
				position: 'absolute',
				top: '-.8em',
				fontSize: 70,
				transform: 'rotate(90deg)',
				cursor: 'pointer',
				animation: 'arrowDown 2s ease-in-out infinite'
			}}
			/>
			</a>
		</EmpresaStyled>
	)
}

export default Empresa