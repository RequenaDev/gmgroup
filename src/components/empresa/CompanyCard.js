import React from 'react';
import styled from 'styled-components';

const CompanyCardStyled = styled.div`
	width: 100%;
	min-height: 240px;
	max-height: 100%;
	border-radius: 20px;
	background: #387e94;
	background-color: #05153d;
	background-blend-mode: screen, overlay;
	box-sizing: border-box;
	position: relative;
	overflow: hidden;
	box-sizing: border-box;
	transition: 2s;
	background: var(--companyCardBg);
	img {
		width: 100px;
		height: 100px;
		display: block;
		margin: auto;
		padding-top: 1.6em;
	}
	h4 {
		text-transform: uppercase;
		display: block;
		text-align: center;
		padding-top: .6em;
		color: var(--textContrast);
	}
	.back-card {
		width: 100%;
		height: 100%;
		border-radius: 20px;
		transform: translateY(0);
		background-image: var(--ccBackBg);
		display: flex;
		justify-content: center;
		align-items: center;
		padding: 0;
		position: absolute;
		opacity: 0;
		top: 0;
		margin: 0;
		transition: .3s;
		&:hover {
			opacity: 1;
		}
	}
	.back-text {
		color: var(--white);
		position: absolute;
		font-size: 16px;
		font-weight: 300;
		padding: 2em;
		margin: 0;
	}
	@media screen and (max-width: 375px) {
		width: 92%;
		margin: auto;
		.back-text {
			font-size: 14px;
		}
	}
`;

function CompanyCard({ imgUrl, text, backText }) {
	return (
		<CompanyCardStyled>
		<div className="fron-card">
			<img src={imgUrl} alt="" />
			<h4>{text}</h4>
		</div>
		<div className="back-card">
			<p className="back-text">{backText}</p>
		</div>
		</CompanyCardStyled>
	)
}

export default CompanyCard