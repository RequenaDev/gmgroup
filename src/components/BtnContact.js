import React from 'react';
import styled from 'styled-components';

const BtnContactStyled = styled.div`
	width: 200px;
	font-size: 1rem;
	color: var(--tcReverse);
	font-weight: 600;
	background-image: var(--contactBtnBg);
	padding: 1em 2em;
	border-radius: 10px;
	margin: 0 auto;
	margin-top: 2em;
	border-bottom: 2px solid rgba(0,0,0, .44);
	box-sizing: border-box;
	cursor: pointer;
	text-align: center;
	opacity: .9;
	transition: .050s;
	&:hover{
		opacity: 1;
	}
`;

function BtnContact({text, clases, handleToggleModal}) {
	return (
		<BtnContactStyled className={clases} onClick={handleToggleModal}>
			{text}
		</BtnContactStyled>
	)
}

export default BtnContact