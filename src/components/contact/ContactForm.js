import React, { useState } from 'react';
import styled from 'styled-components';
import emailjs from 'emailjs-com';

const ContactFormStyled = styled.div`
	width: 100%;
	height: 100%;
	form {
		width: 90%;
		height: 100%;
		margin: auto;
		display: flex;
		flex-direction: column;
		row-gap: .6em;
		padding-top: 2.6em;
		box-sizing: border-box;
		input {
			border: 1px solid #b6bbbf;
			width: 100%;
			outline: none;
			font-size: 1rem;
			padding: .7em 0;
			text-indent: .4em;
			border-radius: 10px;
		}
		.btn-send {
			background: var(--contactBtnBg);
			border: none;
			color: var(--white);
			padding: 1.2em 0;
			cursor: pointer;
			font-weight: 600;
			transition: .2s;
			&:hover {
				opacity: .9;
			}
		}
		textarea {
			border: 1px solid #b6bbbf;
			width: 99%;
			border-radius: 10px;
			resize: none;
			outline: none;
		}
	}
`;

const initialForm = {
	name: '',
	phone: '',
	email: '',
	city: '',
	message: ''
};

function ContactForm({ language }) {
	const [contactForm, setContactForm] = useState(initialForm);

	const handleChangeForm = (e) => {
		const { name, value } = e.target;

		setContactForm({
			...contactForm,
			[name]: value
		});
	};

	function sendEmail(e) {
    e.preventDefault();

    emailjs.sendForm('service_n87339k', 'template_5uuk28i', e.target, 'user_ekm2sYhP0iSxeXkdaEt2w')
      .then((result) => {
          console.log(result.text);
      }, (error) => {
          console.log(error.text);
      });
      e.target.reset();
      setContactForm(initialForm);
  }

	return (
		<ContactFormStyled>
			<form onSubmit={sendEmail}>
				<input
				type="text"
				placeholder={language === 'ES' ? 'Nombre' : 'Name'}
				name="name"
				value={contactForm.name}
				onChange={handleChangeForm}
				required
				 />
				<input
				type="text"
				placeholder={language === 'ES' ? 'Telefono' : 'Phone'}
				name="phone"
				value={contactForm.phone}
				onChange={handleChangeForm}
				required
				 />
				<input
				type="text"
				placeholder={language === 'ES' ? 'Correo electronico' : 'Email'}
				name="email"
				value={contactForm.email}
				onChange={handleChangeForm}
				required
				 />
				<input
				type="text"
				placeholder={language === 'ES' ? 'Ciudad' : 'City'}
				name="city"
				value={contactForm.city}
				onChange={handleChangeForm}
				required
				/>
				<textarea
				placeholder={language === 'ES' ? 'Mensaje' : 'Message'}
				name="message"
				id=""
				cols="30"
				rows="5"
				value={contactForm.message}
				onChange={handleChangeForm}
				required
				>
				</textarea>
				<input
				type="submit"
				value={language === 'ES' ? 'Enviar' : 'Send'}
				className="btn-send"
				/>
			</form>
		</ContactFormStyled>
	)
}

export default ContactForm;