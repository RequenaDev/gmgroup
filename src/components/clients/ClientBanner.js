import React from 'react';
import styled from 'styled-components';

const ClientBannerStyled = styled.div`
	width: 100%;
	height: auto;
	margin-top: 6em;
	display: flex;
	justify-content: center;
	flex-direction: column;
	background-image: var(--bg-countryDos);
	clip-path: polygon(0 0, 100% 0, 100% 88%, 0 100%);
	padding: 2em 0 3em 0;
	h2 {
		display: block;
		text-align: center;
		font-size: 80px;
		background-image: var(--titleGradient);
		-webkit-background-clip: text;
  		-webkit-text-fill-color: transparent;
		text-shadow: 5px 5px 10px rgba(0,0,0, .2);
	}
	button {
		display: flex;
		justify-content: center;
		align-items: center;
		width: 200px;
		height: 40px;
		margin: 0 auto;
		padding: 1.6em;
		font-size: 20px;
		font-weight: 700;
		border-radius: 10px;
		background-image: var(--contactBtnBg);
		color: var(--white);
	}
	@media screen and (max-width: 375px) {
		h2 {
			font-size: 60px;
		}
	}
`;

function ClientBanner({ handleToggleModal, language }) {
	return (
		<ClientBannerStyled>
			<h2>{
				language === 'ES'
				? 'Ellos ya confian en nosotros'
				: 'They already trust us'
			}</h2>
			<button onClick={handleToggleModal}>
			{
				language === 'ES'
				? 'Consultanos'
				: 'Ask us'
			}
			</button>
		</ClientBannerStyled>
	)
}

export default ClientBanner;