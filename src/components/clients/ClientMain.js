import React from 'react';
import styled from 'styled-components';

const ClientMainStyled = styled.div`
	width: 100%;
	height: auto;
	position: relative;
	margin-bottom: 10em;
	h2 {
		display: block;
		text-align: center;
		margin-top: 1em;
		font-size: 60px;
		background-image: var(--titleGradient);
  		-webkit-background-clip: text;
  		-webkit-text-fill-color: transparent;
  		z-index: 20;
	}
	.container-video {
		max-width: 1300px;
		display: flex;
		margin: 0 auto;
		justify-content: space-between;
		align-items: center;
		padding: 0 4em;
		padding-top: 4em;
		column-gap: 2em;
		p {
			width: 30%;
			.name {
				color: #13587a;
				font-weight: 600;
			}
			.com-name {
				color: #60d0b1;
			}
		}
		&::before {
			content: '';
			display: inline-block;
			background: var(--squareBg);
			position: absolute;
			width: 300px;
			height: 300px;
			top: -1em;
			left: -8em;
			border-radius: 10%;
			transform: rotate(45deg);
			opacity: .65;
		}
		.all-company-text {
			color: var(--textContrast);
		}
	}
	@media screen and (max-width: 1000px) {
		.container-video {
			flex-direction: column;
			row-gap: 2em;
			p {
				width: 50%;
			}
			iframe {
				width: 600px;
			}
		}
	}
	@media screen and (max-width: 765px) {
		.container-video {
			p {
				width: 100%;
			}
			iframe {
				width: 575px;
			}
		}
	}
	@media screen and (max-width: 512px) {
		.container-video {
			p {
				width: 100%;
			}
			iframe {
				width: 375px;
			}
			&::before {
				display: none;
			}
		}
	}
`;

function ClientMain({ language }) {
	return (
		<ClientMainStyled id="franquicia">
			<h2>
			{
				language === 'ES'
				? 'Expertos en Cobranza'
				: 'Collection experts'
			}
			</h2>
			<div className="container-video">
				<p className="all-company-text">
					{
						language === 'ES'
						? '"…Todas las empresas hoy necesitamos un servicio de cobranza…"'
						: '"... All companies today need a collection service ..."'
					}
					<br />
					<br />
					<span className="name">EDGAR RUBIDO</span>
					<br />
					<br />
					<span className="com-name">MYM AUTOMÓVILES - URUGUAY</span>
				</p>
				<iframe width="783" height="400" src="https://www.youtube.com/embed/H43Opl9eUHE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</ClientMainStyled>
	)
}

export default ClientMain