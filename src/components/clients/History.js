import React from 'react';
import styled from 'styled-components';
import PersonIcon from '@material-ui/icons/Person';

const HistoryStyled = styled.article`
	width: 420px;
	height: 280px;
	border-radius: 14px;
	padding: 1em 1.4em;
	box-sizing: border-box;
	margin: auto;
	background: var(--clientCard);
	margin-top: ${({row}) => row === 'rowDos' ? '2.4em' : 0};
	width: ${({row}) => row === 'rowTres' || row === 'rowUno' ? '440px' : ''};
	height: ${({row}) => row === 'rowTres' || row === 'rowUno' ? '320px' : ''};
	${({row}) => row === 'rowTres' || row === 'rowUno'
	? 'box-shadow: 4px 7px 7px rgba(0,0,0, .3);'
	: ''
	}
	animation: moveCard ease-in-out 20s infinite;
	.header {
		display: flex;
		justify-content: space-between;
		column-gap: .8em;
		.avatar {
			width: 80px;
			height: 80px;
			background: var(--avatarBg);
			border-radius: 50%;
			display: flex;
			justify-content: center;
			align-items: center;
			overflow: hidden;
		}
		.data {
			margin: 0;
			padding: 0;
			width: 80%;
			display: flex;
			flex-direction: column;
			justify-content: center;
			align-items: flex-start;
			font-size: .8rem;
			row-gap: .4em;
			color: var(--textContrast);
			.company-name {
				font-size: .96rem;
				font-weight: 700;
			}
			.country-data {
				background-image: var(--secundaryTitle);
  				-webkit-background-clip: text;
  				-webkit-text-fill-color: transparent;
			}
		}
	}
	.testimonio {
		font-size: 17px;
		font-weight: 200;
		color: var(--textContrast);
	}
	@media screen and (max-width: 765px) {
		width: 370px;
		height: 280px;
		width: ${({row}) => row === 'rowTres' || row === 'rowUno' ? '380px' : ''};
		height: ${({row}) => row === 'rowTres' || row === 'rowUno' ? '320px' : ''};
		.testimonio {
			font-size: 15px;
		}
	}
	@media screen and (max-width: 375px) {
		width: 320px;
		height: 260px;
		width: ${({row}) => row === 'rowTres' || row === 'rowUno' ? '340px' : ''};
		height: ${({row}) => row === 'rowTres' || row === 'rowUno' ? '280px' : ''};
	}

	@keyframes moveCard {
		0% {
			transform: translateY(6px)
		}
		10% {
			transform: translateY(16px)
		}
		20% {
			transform: translateY(6px)
		}
		30% {
			transform: translateY(16px)
		}
		40% {
			transform: translateY(6px)
		}
		50% {
			transform: translateY(16px)
		}
		60% {
			transform: translateY(6px)
		}
		70% {
			transform: translateY(16px)
		}
		80% {
			transform: translateY(6px)
		}
		90% {
			transform: translateY(16px)
		}
		100% {
			transform: translateY(6px)
		}
	}
`;

function History({ info, company, country, color, row }) {
	return (
		<HistoryStyled color={color} row={row}>
		<header className="header">
		<div className="avatar">
			<PersonIcon
			style={{fontSize: 90, color: '#f3f3f3', borderRadius: '50%', marginTop: '.22em'}}
			/>
		</div>
		<p className="data">
		<span className="company-name">{company}</span>
		<span className="country-data">{country}</span>
		</p>
		</header>
		<p className="testimonio">
			{info}
		</p>
		</HistoryStyled>
	)
}

export default History;