import React from 'react';
import styled from 'styled-components';
import History from './History';

const HistorysStyled = styled.div`
	width: 100%;
	max-width: 1400px;
	height: auto;
	box-sizing: border-box;
	padding: 2em 0;
	grid-gap: 1em;
	display: grid;
	grid-template-columns: repeat(auto-fill, minmax(400px, 1fr));
	justify-content: center;
	margin: 0 auto;
	margin-top: -2em;
	column-gap: 2em;
	overflow: hidden;
	@media screen and (max-width: 1000px) {
		margin-top: 16em;
	}
	@media screen and (max-width: 375px) {
		margin-top: 20em;
	}
`;

function Historys({ language }) {
	return (
		<HistorysStyled id="clients">
		<History
		company="AGROVETERINARIA LA PRADERA"
		country="Florida, Uruguay"
		info={
			language === 'ES'
			? '"Con GM Group hemos logrado un buen vínculo, le tengo mucha confianza. Sé que es una empresa seria y esto me da mucha tranquilidad en el momento de pasar nuestros deudores. En resumen, podemos pensar que realizan una buena gestión seria y responsable, defendiendo nuestros intereses..."'
			: '"With GM Group we have achieved a good bond, I have a lot of confidence in him. I know its a serious company and this gives me a lot of peace of mind when it comes to passing our debtors. In short, we can think that they carry out a serious and responsible good management, defending our interests ..."'
		}
		color="bgDos"
		row="rowUno"
		/>
		<History
		company="FINANCIERA AWA "
		country="Uruguay"
		info={
			language === 'ES'
			? '"Contratamos el servicio hace dos años y estamos muy conformes con los resultados y con la dinámica de trabajo"'
			: '"We hired the service two years ago and we are very satisfied with the results and with the dynamics of work"'
		}
		color="bgUno"
		row="rowDos"
		/>
		<History
		company="JAVIER WAGNER (particular)"
		country="Buenos Aires, Argentina"
		info={
			language === 'ES'
			? '"El servicio me pareció excelente ya que por otros medios no hubiese conseguido cobrar esa deuda. Considero que es un servicio altamente recomendable por eso se lo estoy recomendando a la gente de mi entorno que tiene este tipo de inconvenientes."'
			: '"I found the service excellent because by other means I would not have been able to collect that debt. I consider it a highly recommended service so I am recommending it to people around me who have this type of inconvenience."'
		}
		color="bgDos"
		row="rowTres"
		/>
		<History
		company="COLEGIO INSTITUTO SAAVEDRA"
		country="Córdoba, Argentina"
		info={
			language === 'ES'
			? '"La verdad es que para el poco tiempo que estamos trabajando con ustedes y las circunstancias actuales, tenemos una buena opinión ya que prontamente tuvimos resultados de sus gestiones. Estamos preparando más documentación para acercarles y continuar trabajando con ustedes."'
			: '"The truth is that for the short time we are working with you and the current circumstances, we have a good opinion since we soon had results of your efforts. We are preparing more documentation to bring you closer and continue to work with you."'
		}
		color="bgUno"
		row="rowUno"
		/>
		<History
		company="INMOBILIARIA FRANCISCO RAYA"
		country="Entre Ríos, Argentina"
		info={
			language === 'ES'
			? '"Excelente servicio!!!"'
			: '"Excellent service!!!"'
		}
		color="bgDos"
		row="rowDos"
		/>
		<History
		company="DINELEC"
		country="Salta, Argentina"
		info={
			language === 'ES'
			? '"La verdad quede muy conforme con el servicio. Me gusto mucho la versatilidad que tienen para ver la manera de que ambas partes logremos el objetivo. Tuve muy buena respuesta en la primera tanda de deudores que mandamos y ahora estamos trabajando en la segunda."'
			: '"The truth is very satisfied with the service. I really enjoyed the versatility they have to see how both sides achieve the goal. I had a very good response in the first batch of debtors that we sent and now we are working on the second."'
		}
		color="bgUno"
		row="rowTres"
		/>

		</HistorysStyled>
	)
}

export default Historys;