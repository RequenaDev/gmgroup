import React from 'react';
import styled from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import signoInterrogacion from 'assets/signo-de-interrogacion.png';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
}));

const QuestionsStyled = styled.div`
	width: 100%;
  max-width: 1800px;
  margin: 0 auto;
  padding:0;
	height: auto;
	display: grid;
	grid-template-columns: 1fr 1fr;
	margin-top: 4em;
  span {
      background-image: var(--secundaryTitle);
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
  }
	.question-icon {
		h2 {
			display: block;
			text-align: center;
			font-size: 50px;
			background-image: var(--titleGradient);
			-webkit-background-clip: text;
  		-webkit-text-fill-color: transparent;
		}
	}
	.icon-quest {
		display: block;
		margin: auto;
		width: 400px;
		height: 360px;
	}
  .question-container {
    width: 90%;
    margin: 0;
    margin-top: 2em;
    display: flex;
    align-items: center;
    padding: 0;
    padding-left: 1em;
  }
	.acordeon {
		background-image: var(--questionsBg);
		display: flex;
		flex-direction: column;
    .t-cobranza {
			background: transparent;
			font-size: 18px;
		}
		.t-quest {
      font-weight: 600;
		}
    .MuiPaper-root {
      background: var(--questionBg);
      color: var(--textContrast);
    }
    .MuiIconButton-root {
      color: var(--textContrast);
    }
    span {
      background-image: var(--secundaryTitle);
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    }
	}
 
  @media screen and (max-width: 765px) {
    grid-template-columns: 1fr;
    grid-template-rows: 1fr 1fr;
  }
  @media screen and (max-width: 375px) {
    height: auto;
    .question-icon {
      height: 400px;
    }
    .icon-quest {
      width: 300px;
      height: 400px;
    }
    .acordeon {
      width: 320px;
      margin: 0 auto;
    }

  }
`;

function Questions({ language }) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };
	return (
		<QuestionsStyled>
  		<div className="question-icon">
  		<h2>
      {
        language === 'ES'
        ? 'Preguntas frecuentes'
        : 'Frequent questions'
      }
      </h2>
  		<img
  		className="icon-quest"
  		src={signoInterrogacion}
  		alt=""
  		/>
  		</div>

    <div className="question-container">
  <div className={classes.root && 'acordeon'}>
      <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <Typography className={classes.heading && 't-quest'}>
          {
            language === 'ES'
            ? '¿Que tipo de cobranzas realizan?'
            : 'What type of collections do you carry out?'
          }
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="t-cobranza">
          {
            language === 'ES'
            ? 'Somos especialistas en servicios de cobranza. Con más de 10 años de experiencia nuestro equipo de profesionales se encarga de proveer una solución para el recupero de dinero.'
            : 'We are specialists in collection services. With more than 10 years of experience, our team of professionals is in charge of providing a solution for the recovery of money.'
          }
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2bh-content"
          id="panel2bh-header"
        >
          <Typography className={classes.heading && 't-quest'}>
          {
            language === 'ES'
            ? '¿En donde trabaja GM Group?'
            : 'Where does GM Group work?'
          }
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="t-cobranza">
          {
            language === 'ES'
            ? 'Trabajamos a nivel global, principalmente contamos con equipos propios de profesionales expertos en cobranza en Argentina y Uruguay. Así como también poseemos partners en Brasil, Chile y Paraguay.'
            : 'We work globally, mainly we have our own teams of professional collection experts in Argentina and Uruguay. As well as we have partners in Brazil, Chile and Paraguay.'
          }
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel3bh-content"
          id="panel3bh-header"
        >
          <Typography className={classes.heading && 't-quest'}>
            {
              language === 'ES'
              ? '¿Quien puede contratar los servicios de cobranza?'
              :  'Who can hire collection services?'
            }
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="t-cobranza">
          {
            language === 'ES'
           ? 'Ofrecemos nuestros servicios a empresas, pymes, instituciones y particulares que cuenten con una deuda y necesiten soporte para lograr el cobro de la misma. Brindamos soluciones integrales a todo aquel que necesite soporte y mejorar la gestión de su cobranza.'
          : 'We offer our services to companies, SMEs, institutions and individuals who have a debt and need support to collect it. We provide comprehensive solutions to anyone who needs support and improve their collection management.'
          }
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === 'panel4'} onChange={handleChange('panel4')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel4bh-content"
          id="panel4bh-header"
        >
          <Typography className={classes.heading && 't-quest'}>
          {
            language === 'ES'
            ? '¿Que tipo de cobranzas realizan?'
            : 'What type of collections do you carry out?'
          }
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="t-cobranza">
          {
            language === 'ES'
            ? <>
            Te ofrecemos soluciones integrales en gestión y cobranza.
           <br />
           <br />
            <b><span>●</span> COBRANZA PREVENTIVA: </b>
            La cobranza preventiva es un servicio que inicia días antes del vencimiento de un crédito y abarca hasta los 30 días posteriores.
            <br />
            <br />
            <b><span>●</span> COBRANZA TARDÍA O EXTRAJUDICIAL: </b>
            Existe la posibilidad de recuperar su cartera vencida en un contexto de negociación personalizada, oportuna y ética.
            <br />
            <br />
            <b><span>●</span> COBRANZA TARDÍA JUDICIAL: </b>
            Una vez agotadas las oportunidades del acuerdo extrajudicial, puede procederse al inicio de acciones judiciales para el cobro de una deuda.
            </>
            : <>
            We offer you comprehensive solutions in management and collection.
           <br />
           <br />
            <span>●</span> <b>PREVENTIVE COLLECTION: </b> Preventive collection is a service that begins days before the expiration of a loan and covers up to 30 days afterwards.
            <br />
            <br />
            <span>●</span> <b>LATE OR EXTRAJUDICIAL COLLECTION: </b>
             There is the possibility of recovering your past due portfolio in a context of personalized, timely and ethical negotiation.
            <br />
            <br />
           <span>●</span> <b>LATE JUDICIAL COLLECTION: </b>
             Once the opportunities of the out-of-court settlement have been exhausted, legal actions can be initiated to collect a debt.
            </>
          }

          </Typography>
        </AccordionDetails>
      </Accordion>
        <Accordion expanded={expanded === 'panel5'} onChange={handleChange('panel5')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel5bh-content"
          id="panel5bh-header"
        >
          <Typography className={classes.heading && 't-quest'}>
          {
            language === 'ES'
            ? '¿Cuanto sale contratar el servicio?'
            : 'How much does it cost to hire the service?'
          }
            </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="t-cobranza">
          {
            language === 'ES'
            ? 'Al contratar el servicio no cobramos adelantos ni costos fijos. Nos pagas una comisión solo si cobramos tu deuda.'
            : 'When hiring the service we do not charge advances or fixed costs. You pay us a commission only if we collect your debt.'
          }
          </Typography>
        </AccordionDetails>
      </Accordion>
        <Accordion expanded={expanded === 'panel6'} onChange={handleChange('panel6')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel6bh-content"
          id="panel6bh-header"
        >
          <Typography className={classes.heading && 't-quest'}>
          {
            language === 'ES'
            ? '¿Que significa que trabajen a comision?'
            : 'What does it mean that they work on commission?'
          }
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="t-cobranza">
          {
            language === 'ES'
            ? 'Una vez lograda la cobranza, nos abonan un porcentaje de comisión establecido al momento de la contratación.'
            : 'Once the collection is achieved, they pay us a commission percentage established at the time of hiring.'
          }
          </Typography>
        </AccordionDetails>
      </Accordion>
        <Accordion expanded={expanded === 'panel7'} onChange={handleChange('panel7')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel7bh-content"
          id="panel7bh-header"
        >
          <Typography className={classes.heading && 't-quest'}>
          {
            language === 'ES'
            ? '¿Es efectiva la cobranza?'
            : 'Is the collection effective?'
          }
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="t-cobranza">
          {
            language === 'ES'
            ? 'Garantizamos los mejores índices de recupero en el mercado. Actuamos en todo tipo de atrasos, manteniendo clientes activos y reduciendo morosidad.'
            : 'We guarantee the best recovery rates in the market. We act on all types of arrears, keeping clients active and reducing delinquencies.'
          }
          </Typography>
        </AccordionDetails>
      </Accordion>
        <Accordion expanded={expanded === 'panel8'} onChange={handleChange('panel8')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel8bh-content"
          id="panel8bh-header"
        >
          <Typography className={classes.heading && 't-quest'}>
          {
            language === 'ES'
            ? '¿Como realizan la cobranza?'
            : 'How do you collect?'
          }
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="t-cobranza">
          {
            language === 'ES'
            ? 'Ofrecemos servicios especializados y personalizados para el recupero y cobranza de deudas en todas sus etapas, mediante un equipo de gestores operadores y abogados con amplia experiencia. Contamos con profesionales de amplia trayectoria en cobranza telefónica y cobranzas de campo, en materia jurídica, administrativa, financiera y de sistemas. La esencia de nuestro negocio está unida al conocimiento y destreza de nuestro personal, generando valor a nuestros servicios. Liderazgo y servicios de calidad nos distinguen.'

            : 'We offer specialized and personalized services for the recovery and collection of debts in all its stages, through a team of managers, operators and lawyers with extensive experience. We have professionals with extensive experience in telephone collections and field collections, in legal, administrative, financial and systems matters. The essence of our business is linked to the knowledge and skills of our staff, generating value for our services. Leadership and quality services distinguish us.'

          }
          </Typography>
        </AccordionDetails>
      </Accordion>
        <Accordion expanded={expanded === 'panel9'} onChange={handleChange('panel9')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel9bh-content"
          id="panel9bh-header"
        >
          <Typography className={classes.heading && 't-quest'}>
          {
            language === 'ES'
            ? '¿Cuales son los beneficios de contratar los servicios de GM Group?'
            : 'What are the benefits of hiring the services of GM Group?'
          }
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className="t-cobranza">
          {
            language === 'ES'
            ?<>
            ● Elaboración de estrategias para la gestión y cobro de deudas.
            <br />
            ● Elaboración de plan de cobranza flexible y acorde a las necesidades de cada empresa o particular.
            <br />
            ● Facilidad y comodidad de contratación.
            <br />
            ● Comunicación constante.
            <br />
            ● Reportes sistematizados.
            <br />
            ● Trato directo y personalizado.
            <br />
            ● Gestiones de cobranza realizadas por profesionales en cobranzas y con la tecnología más innovadora del mercado.
            <br />
            ● Efectividad en gestión y cobro de deudas.
            <br />
            ● Es más económico. Reducción de costos de cobranza directos e indirectos para su negocio.
            <br />
            ● Trabajamos por objetivos. No pedimos adelanto, nos abonas solo si cobramos tu deuda.
            <br />
            ● Nos enfocamos en cuidar tus vínculos durante la gestión. Queremos que mantengas una buena relación con tus clientes.
            <br />
            ● Presencia Regional: oficinas en Argentina, Uruguay y alianzas estratégicas en Brasil, Chile y Paraguay.
            </>
            : <>
               <span>●</span> Development of strategies for the management and collection of debts.
               <br />
               <span>●</span> Development of a flexible collection plan according to the needs of each company or individual.
               <br />
               <span>●</span> Ease and convenience of hiring.
               <br />
               <span>●</span> Constant communication.
               <br />
               <span>●</span> Systematized reports.
               <br />
               <span>●</span> Direct and personalized treatment.
               <br />
               <span>●</span> Collection procedures carried out by collection professionals and with the most innovative technology on the market.
               <br />
               <span>●</span> Effectiveness in debt management and collection.
               <br />
               <span>●</span> It is cheaper. Reduction of direct and indirect collection costs for your business.
               <br />
               <span>●</span> We work by objectives. We do not ask for advance, you pay us only if we collect your debt.
               <br />
               <span>●</span> We focus on taking care of your ties during management. We want you to maintain a good relationship with your customers.
               <br />
               <span>●</span> Regional Presence: offices in Argentina, Uruguay and strategic alliances in Brazil, Chile and Paraguay.
            </>
          }
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
    </div>
		</QuestionsStyled>
	)
}

export default Questions;