import React from 'react';
import styled from 'styled-components';
import BtnContact from 'components/BtnContact';

const QuestionsBannerStyled = styled.div`
	width: 100%;
	height: 40vh;
	height: auto;
	margin-top: 5em;
	box-shadow: 0px 0px -30px #2b6f8bb1;
	position: relative;
	overflow: hidden;
	padding: 5em 0;
	&::before, &::after {
		content: '';
		display: inline-block;
		background: transparent;
		position: absolute;
		width: 360px;
		height: 360px;
		top: 1.2em;
		right: -6em;
		border-radius: 50%;
		transform: rotate(65deg);
		opacity: .65;
		box-shadow: 1px 1px 2px #80d0c7, 1px 1px 2px #80d0c7;
	}
	&::after {
		width: 320px;
		height: 280px;
		left: -10em;
		transform: rotate(25deg);
	}
	h2 {
		width: 70%;
		display: block;
		margin: auto;
		text-align: center;
		font-size: 40px;
		background-image: var(--titleGradient);
  		-webkit-background-clip: text;
  		-webkit-text-fill-color: transparent;
		padding-top: 1em;
	}
	@media screen and (max-width: 800px) {
		padding: 0 0 4em 0;
		h2 {
			width: 90%;
		}
	}
	@media screen and (max-width: 1000px) {
		&::before, &::after {
			display: none;
		}
	}
	@media screen and (max-width: 375px) {
		h2 {
			width: 90%;
			font-size: 30px;
		}
	}
`;

function QuestionsBanner({ handleToggleModal, language }) {
	return (
		<QuestionsBannerStyled>
			<h2>
			{
			language === 'ES'
			? '¿Necesitás mejorar la gestión de tu cobranza? ¡Nosotros podemos ayudarte!'
			: 'Do you need to improve your collection management? We can help you!'
			}</h2>
        	<BtnContact
		text={language === 'ES' ? 'Contactanos' : 'Contact us'}
		handleToggleModal={handleToggleModal}
		/>
		</QuestionsBannerStyled>
	)
}

export default QuestionsBanner