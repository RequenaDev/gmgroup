import React from 'react';
import styled from 'styled-components';
import ContactForm from 'components/contact/ContactForm';
import CloseIcon from '@material-ui/icons/Close';
import bussinesImage from 'assets/bussines.jpg';

const ConsultModalStyled = styled.div`
	position: fixed;
	top: 0;
	width: 100%;
	height: 100vh;
	background: rgba(0,0,0, .4);
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: column;
	margin: 0;
	padding: 0;
	.modal {
		width: 700px;
		height: 500px;
		display: flex;
		border-radius: 20px;
		align-items: center;
		position: relative;
		background: var(--bodyBg);
		.modal-image {
			background-image: linear-gradient(to right, #243949b9 0%, #517fa4b9 100%), url(${bussinesImage});
			width: 100%;
			height: 100%;
			object-fit: cover;
			border-radius: 20px;
			p {
				background-image: var(--questionsBg);
				-webkit-background-clip: text;
  				-webkit-text-fill-color: transparent;
				display: block;
				text-align: center;
				font-size: 28px;
				padding: 0 .8em;
			}
			strong {
				color: var(--white);
				display: block;
				text-align: center;
				font-size: 54px;
			}
			.we-can-help-you {
				color: var(--gray);
				font-size: 18px;
				margin-top: 3em;
			}
		}
		.box-close-icon {
			position: absolute;
			right: 2.6em;
			top: -1.8em;
		}
	}
	@media screen and (max-width: 800px) {
		.modal-image {
			display: none;
		}
		.modal {
			width: 400px;
		}
	}
	@media screen and (max-width: 375px) {
		.modal {
			width: 300px;
		}
	}
`;

const closeStyle = {
	color: "#666666",
	cursor: "pointer",
	position: 'absolute',
	top: '1em',
	right: '1em,',
	fontSize: 40
}

function ConsultModal({ handleToggleModal, language }) {
	return (
		<ConsultModalStyled>
			<div className="modal">
			<div className="box-close-icon">
			<CloseIcon
			className="close-icon"
			style={closeStyle}
			onClick={handleToggleModal}
			/>
			</div>
			<div className="modal-image">
			<p>¿Tienes deudas por cobrar?</p>
			<strong>Solicita una consultoria sin costo</strong>
			<p className="we-can-help-you">NOSOTROS PODEMOS AYUDARTE</p>
			</div>
			<ContactForm
			language={language}
			/>
			</div>
		</ConsultModalStyled>
	)
}

export default ConsultModal;