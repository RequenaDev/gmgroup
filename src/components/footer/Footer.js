import React from 'react';
import styled from 'styled-components';
import ContactForm from 'components/contact/ContactForm';
import RoomOutlinedIcon from '@material-ui/icons/RoomOutlined';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import logoLight from 'assets/logo-light.png';
import YouTubeIcon from '@material-ui/icons/YouTube';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';

const FooterStyled = styled.div`
	background: #2e4454;
	background: var(--footerBg);
	width: 100%;
	height: auto;
	margin-top: 4em;
	position: relative;
	.container {
		width: 90%;
		margin: 0 auto;
		display: grid;
		grid-template-columns: repeat(auto-fill, minmax(33%, 1fr));
	}
	.box-logotipo {
		display: flex;
		flex-direction: column;
		img {
			display: block;
			margin: 0 auto;
			width: 80%;
			height: 50%;
		}
		.social-media {
			ul {
				width: 50%;
				padding: 0;
				margin: 0 auto;
				display: flex;
				flex-direction: column;
				align-items: center;
				justify-content: space-between;
				row-gap: .6em;
				li {
					display: flex;
					align-items: center;
					list-style: none;
					min-width: 100%;
					padding: .3em 0em .3em .6em;
					background: var(--smBg);
					border-radius: 10px;
					&:hover {
						background: var(--sMHover);
					}
					a {
						text-decoration: none;
						color: #fff;
						width: 100%;
						display: flex;
						align-items: center;
						font-size: 13px;
						column-gap: .4em;
					}
				}
			}
		}
		p {
			position: absolute;
			font-size: 14px;
			left: 1em;
			bottom: 0em;
			padding: 0;
			margin:0;
			color: var(--white);
			opacity: 1;
			a {
				color: var(--white);
				text-decoration: none;
				opacity: 1;
				&:hover {
					opacity: .8;
				}
			}
		}
	}
	.numeros {
		width: 300px;
		height: 40%;
		margin: auto;
		margin-top: 5em;
		display: flex;
		flex-direction: column;
		row-gap: 2em;
		align-items: center;
		justify-content: space-around;
		.num-arg, .num-uru {
			width: 100%;
			display: flex;
			justify-content: center;
			flex-direction: column;
		}
		strong {
			color: var(--white);
			display: flex;
			align-items: center;
		}
	}
	@media screen and (min-width: 1400px) {
		.container {
			width: 1400px;
		}
	}
	@media screen and (max-width: 765px) {
		.container {
			grid-template-columns: 1fr;
			grid-template-rows: 400px 300px 500px;
		}
	}
`;

function Footer({ language }) {
	return (
		<FooterStyled id="contact">
			<div className="container">
			<div className="box-logotipo">
				<img src={logoLight} alt="" />
				<div className="social-media">
					<ul>
						<li>
							<a 
							href="https://www.facebook.com/gestionmorosos" 
							rel="noreferrer"
							target="_blank">
								<FacebookIcon style={{fontSize: 25, color: '#0791e6'}} />
								Facebook
							</a>
						</li>
						<li>
							<a 
							href="https://www.youtube.com/channel/UCJoMCy-iDD5jXtoa15WDaXQ"
							rel="noreferrer"
							target="_blank">
							
								<YouTubeIcon style={{fontSize: 25, color: '#ff4e4e'}} />
								Youtube
							</a>
						</li>
						<li>
							<a 
							href="https://www.linkedin.com/company/33182670/admin/"
							rel="noreferrer"
							target="_blank">
							
								<LinkedInIcon style={{fontSize: 25, color: '#0791e6'}} />
								LinkedIn
							</a>
						</li>
						<li>
							<a 
							href="https://www.instagram.com/gestionmorosos/"
							rel="noreferrer"
							target="_blank">
							
								<InstagramIcon style={{fontSize: 25, color: '#c85fbf'}} />
								Instagram
							</a>
						</li>
					</ul>
				</div>
				{
					language === 'ES'
					? <p>© 2021 GM Group | <a href="http://zevensoftware.com/" target="a_blank">Desarrollado por por www.zevensoftware.com</a> | Todos los derechos reservados.</p>
					: <p>© 2021 GM Group | <a href="http://zevensoftware.com/" target="a_blank">Developed by www.zevensoftware.com</a> | All rights reserved.</p>
				}
			</div>
			<div className="numeros">
				<div className="num-arg">
				<strong><RoomOutlinedIcon /> ARGENTINA</strong>
				<strong><WhatsAppIcon /> (+54) 9 345 408 1835</strong>
				</div>
				<div className="num-uru">
				<strong><RoomOutlinedIcon /> URUGUAY</strong>
				<strong><WhatsAppIcon /> 0 800 9971</strong>
				</div>
			</div>
			<ContactForm language={language} />
			<br />
			</div>
		</FooterStyled>
	)
}

export default Footer;