import React from 'react';
import styled from 'styled-components';
import CarrouselNews from './CarrouselNews';
import NewsMobile from './NewsMobile';

const NewsStyled = styled.div`
	max-width: 1000px;
	margin: auto;
	height: auto;
	h2 {
		font-size: 70px;
		display: block;
		text-align: center;
		background-image: var(--titleGradient);
  		-webkit-background-clip: text;
  		-webkit-text-fill-color: transparent;
	}
	@media screen and (max-width: 765px) {
		margin-top: 8em;
		height: auto;
	}
`;

function News({ language }) {
	return (
		<NewsStyled>
			<h2>
			{
				language === 'ES'
				? 'NOTICIAS'
				: 'NEWS'
			}
			</h2>
				<NewsMobile />
				<CarrouselNews />
		</NewsStyled>
	)
}

export default News;