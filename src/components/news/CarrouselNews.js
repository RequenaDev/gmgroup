import React, {useRef, useState} from 'react';
import styled from 'styled-components';
import DoubleArrowRoundedIcon from '@material-ui/icons/DoubleArrowRounded';
import New from './New';
import blgImage1 from 'assets/news-images/Blog-Juan-Manuel-Berdaguer-1-324x160.jpg';
import blgImage2 from 'assets/news-images/inversion-218x150.jpg';
import blgImage3 from 'assets/news-images/convenio-1-324x160.jpg';
import blgImage4 from 'assets/news-images/omnicanal-ajuste-1-1-218x150.jpg';
import blgImage5 from 'assets/news-images/sabrina-blog-741x486.png';
import blgImage6 from 'assets/news-images/segundo-webinar-324x160.jpg';


const CarrouselNewsStyled = styled.div`
  width: 98%;
  height: 100%;
  margin: auto;
  padding: 2em 0;
  display: grid;
  grid-template-columns: 5% 90% 5%;
  .box-arrow-left, .box-arrow-right {
    display: flex;
    justify-content: center;
    align-items: center;
    .arrow-right, .arrow-left {
      transition: .2s;
      color: var(--textContrast);
      &:hover {
        color: #80d0c7;
      }
      &:active {
        transform: scale(.9);
      }
    }
  }
  .box-arrow-left {
    transform: rotateY(180deg);
  }
  .container {
    overflow: hidden;
    display: flex;
    column-gap:.6em;
    flex-wrap: no-wrap;
  }
  .news {
    width: 100%;
    display: flex;
    transition: .5s;
  }
  .carrPages-2 {
    transform: translateX(-890px);
  }
  @media screen and (max-width: 1000px) {
    display: none;
  }
`;

function CarrouselNews() {
  const [carrPages, setCarrPages] = useState(1);
  const btnLeftRef = useRef();
  const btnRightRef = useRef();
  const newsRef = useRef();

  const handleBtnLeft = (e) => {
    if (carrPages === 2) {
      setCarrPages(1)
      newsRef.current.classList.remove('carrPages-2');
    }
  };

  const handleBtnRight = (e) => {
    newsRef.current.classList.add('carrPages-2');
    setCarrPages(2);
  };

  return (
    <CarrouselNewsStyled>
    <div className="box-arrow-left">
      <DoubleArrowRoundedIcon
      ref={btnLeftRef}
      className="arrow-left"
      onClick={handleBtnLeft}
      style={{fontSize: 50, cursor: 'pointer'}} />
    </div>
    <div className="container">
      <div className="news" ref={newsRef}>
      <New
      image={blgImage1}
      category="Gestion Morosos"
      title="Conoce a Juan Manuel Berdaguer de la..."
      relVlog="https://blog.gestionmorosos.com/conoce-a-juan-manuel-berdaguer-de-la-franquicia-buenos-aires/"
      />
      <New
      image={blgImage2}
      category="Consejo Financiero"
      title="Como y en que invertir en este momento"
      relVlog="https://blog.gestionmorosos.com/como-y-en-que-invertir-en-este-momento/"

      />
      <New
      image={blgImage3}
      category="Gestion Morosos"
      title="Nuevo convenio GM Group"
      relVlog="https://blog.gestionmorosos.com/nuevo-convenio-gm-group/"
      />
      <New
      image={blgImage4}
      category="Finanzas Personales"
      title="Omnicanalidad y transformación..."
      relVlog="https://blog.gestionmorosos.com/omnicanalidad-y-transformacion-digital-en-las-distintas-generaciones/"

      />
      <New
      image={blgImage5}
      category="Franquicias"
      title="Conocé a Sabrina Jayat de la Agencia"
      relVlog="https://blog.gestionmorosos.com/conoce-a-sabrina-jayat-de-la-agencia-gm-group-salta/"

      />
       <New
      image={blgImage6}
      category="Gestion Morosos"
      title="5 Claves para mejorar mi rendimiento..."
      relVlog="https://blog.gestionmorosos.com/5-claves-para-mejorar-mi-rendimiento-personal/"

      />
      </div>
    </div>
    <div className="box-arrow-right">
      <DoubleArrowRoundedIcon
      ref={btnRightRef}
      onClick={handleBtnRight}
      className="arrow-right"
      style={{fontSize: 50, cursor: 'pointer'}} />
    </div>

    </CarrouselNewsStyled>
  )
}

export default CarrouselNews