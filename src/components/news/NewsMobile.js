import React from 'react';
import styled from 'styled-components';
import New from './New';
import blgImage1 from 'assets/news-images/Blog-Juan-Manuel-Berdaguer-1-324x160.jpg';
import blgImage2 from 'assets/news-images/inversion-218x150.jpg';
import blgImage3 from 'assets/news-images/convenio-1-324x160.jpg';
import blgImage4 from 'assets/news-images/omnicanal-ajuste-1-1-218x150.jpg';
import blgImage5 from 'assets/news-images/sabrina-blog-741x486.png';
import blgImage6 from 'assets/news-images/segundo-webinar-324x160.jpg';

const NewsMobileStyled = styled.div`
	width: 90%;
	height: auto;
	margin: auto;
	display: grid;
	row-gap: 1em;
	column-gap: 1em;
	grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
	@media screen and (min-width: 1000px) {
		display: none;
	}
`;

const NewsMobile = () => {
	return (
		<NewsMobileStyled>
			 <New
      		image={blgImage1}
      		category="Gestion Morosos"
      		title="Conoce a Juan Manuel Berdaguer de la..."
      		relVlog="https://blog.gestionmorosos.com/conoce-a-juan-manuel-berdaguer-de-la-franquicia-buenos-aires/"
      		/>
      		<New
      		image={blgImage2}
      		category="Consejo Financiero"
      		title="Como y en que invertir en este momento"
      		relVlog="https://blog.gestionmorosos.com/como-y-en-que-invertir-en-este-momento/"
      		/>
      		<New
      		image={blgImage3}
      		category="Gestion Morosos"
      		title="Nuevo convenio GM Group"
      		relVlog="https://blog.gestionmorosos.com/nuevo-convenio-gm-group/"
      		/>
      		<New
      		image={blgImage4}
      		category="Finanzas Personales"
      		title="Omnicanalidad y transformación..."
      		relVlog="https://blog.gestionmorosos.com/omnicanalidad-y-transformacion-digital-en-las-distintas-generaciones/"
      		/>
      		<New
      		image={blgImage5}
      		category="Franquicias"
      		title="Conocé a Sabrina Jayat de la Agencia"
      		relVlog="https://blog.gestionmorosos.com/conoce-a-sabrina-jayat-de-la-agencia-gm-group-salta/"
      		/>
      		 <New
      		image={blgImage6}
      		category="Gestion Morosos"
      		title="5 Claves para mejorar mi rendimiento..."
      		relVlog="https://blog.gestionmorosos.com/5-claves-para-mejorar-mi-rendimiento-personal/"
      		/>
		</NewsMobileStyled>
	)
}

export default NewsMobile