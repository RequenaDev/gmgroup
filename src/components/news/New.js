import React from 'react';
import styled from 'styled-components';

const NewsStyled = styled.div`
	min-width: 260px;
	height: 98%;
	padding: 0 1em;
	grid-gap: .2em;
	cursor: pointer;
	border-radius: 5px;
	box-shadow: 1px 3px 10px rgba(0,0,0, .1);
	a {
		text-decoration: none;
	}
	img {
		width: 100%;
		height: 160px;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
	}
	p {
		font-size: 16px;
		padding: .3em 0;
		font-weight: 600;
		margin: 0;
		background-image: var(--secundaryTitle);
		-webkit-background-clip: text;
  		-webkit-text-fill-color: transparent;
	}
	.card-title {
		text-align: start;
		font-size: 24px;
		background-image: var(--textContrastGradient);
		-webkit-background-clip: text;
  		-webkit-text-fill-color: transparent;
	}
	@media screen and (max-width: 375px) {
		padding: 0;
		margin: 0 auto;
		margin-left: 1em;

	}
`;

const New = ({ category, image, title, relVlog }) => {
	return (
		<NewsStyled>
		<a href={relVlog}>
			<img src={image} alt="" />
			<p>{category}</p>
			<h2 className="card-title">{title}</h2>
		</a>
		</NewsStyled>
	)
}

export default New;