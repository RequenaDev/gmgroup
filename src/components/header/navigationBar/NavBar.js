import React from 'react';
import styled from 'styled-components';
import logo from 'assets/GM-Group-Logotipo-min.png';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Brightness2Icon from '@material-ui/icons/Brightness2';
import WbSunnyIcon from '@material-ui/icons/WbSunny';

const NavBarStyled = styled.div`
	width: 100%;
	height: 3em;
	padding-top: .4em;
	position: relative;
	.wrapper {
		max-width: 90%;
		max-height: 100%;
		margin: auto;
		display: flex;
		justify-content: space-between;
	}
	.logo {
		img {
			width: 100px;
			max-height: 100%;
		}
	}
	.pages {
		display: flex;
		width: 100%;
		.list-enlaces {
			width: 100%;
			display: flex;
			align-items: center;
			justify-content: flex-start;
			margin: 0;
			padding: 0;
			padding-left: 1em;
			li {
				list-style: none;
				margin: 0 .5em;
				display: flex;
				padding: 0 .6em;
				justify-content: center;
				position: relative;
				color: var(--textContrast);
				text-align: center;
			}
			a {
				font-size: 16px;
				font-weight: 500;
				text-decoration: none;
				color: var(--textContrast);
			}
		}
	}
	.nav-2 {
		width: 240px;
		display: flex;
		align-items: center;
		justify-content: space-between;
		column-gap: 2em;
		img {
			width: 50px;
			height: 50px;
		}
		.flags {
			display: flex;
			column-gap: 1em;
			.select-language {
				display: flex;
				justify-content: space-between;
				align-items: center;
				border: none;
				outline: none;
				padding: .4em .4em .4em .6em;
				border-radius: 10px;
				font-weight: 600;
				cursor: pointer;
				background-image: var(--contactBtnBg);
				position: relative;
				transition: .2s;
				.list-languages {
					position: absolute;
					top: 2.86em;
					right: 0;
					left:0;
					z-index: 1;
					background-image: var(--contactBtnBg);
					border-radius: 10px;
					display: none;
					.option-language {
						width: 100%;
						height: 100%;
						border-radius: 10px;
						opacity: .8;
						&:hover {
							opacity: 1;
						}
					}
				}
			}
			.select-language:hover .list-languages {
				display: flex;
			}
		}
		button {
			background-image: var(--contactBtnBg);
			color: var(--white);
			font-size: .76rem;
			font-weight: 600;
			padding: .84em .74em;
			border-radius: 10px;
			cursor: pointer;
		}
	}
	@media screen and (max-width: 1000px) {
		.pages {
			display: none;
		}
	}
	@media screen and (max-width: 375px) {
		.nav-2 {
			width: 200px;
			column-gap: 1em;
		}
	}
`;

function NavBar({
	language,
	setLanguage,
	darkMode,
	setDarkMode }) {

	const handleAppTheme = () => {
	setDarkMode(!darkMode)
  	const theme = window.localStorage.getItem('appTheme')
  	if(theme === 'true') {
  		window.localStorage.setItem('appTheme', 'false')
  	}
  	if(theme === 'false') {
  		window.localStorage.setItem('appTheme', 'true')
  	}
  };

  const handleLanAppEn = () => {
	  const lan = window.localStorage.getItem('appLan');

	  if (lan === 'ES') {
		  window.localStorage.setItem('appLan', 'EN')
		  setLanguage('EN')
	  }
  };

  const handleLanAppEs = () => {
	  const lan = window.localStorage.getItem('appLan');

	  if (lan === 'EN') {
		  window.localStorage.setItem('appLan', 'ES')
		  setLanguage('ES')
	  } 
  };

	return (
		<NavBarStyled>
		<div className="wrapper">
			<div className="logo">
				<img src={logo} alt="Logo" style={{cursor: 'pointer'}} />
			</div>
			<div className="pages">
			<ul className="list-enlaces">
				<li><a href="#home">
				{language === 'ES' ? 'Inicio' : 'Home'}
				</a></li>
				<li><a href="#nosotros">
				{language === 'ES' ? 'Nosotros' : 'About us'}
				</a></li>
				<li><a href="#franquicia">
				{language === 'ES' ? 'Franquicia' : 'Franchise'}
				</a></li>
				<li><a href="#clients">
				{language === 'ES' ? 'Clientes' : 'Clients'}
				</a></li>
				<li><a href="#contact">
				{language === 'ES' ? 'Contactanos' : 'Contact us'}
				</a></li>
			</ul>
			</div>
			<div className="nav-2">
			{
				darkMode
				? <WbSunnyIcon
				style={{color: '#fff', cursor: 'pointer'}}
				onClick={handleAppTheme}
				/>
				: <Brightness2Icon
				style={{cursor: 'pointer'}}
				onClick={handleAppTheme}
				/>
			}
				<div className="flags">
					<button className="select-language">
						{language}
						<ArrowDropDownIcon style={{color: `#fff`}} />
						<div className="list-languages">
							<button
							className="option-language"
							onClick={handleLanAppEn}
							>EN
							</button>
							<button
							className="option-language"
							onClick={handleLanAppEs}
							>ES
							</button>
						</div>
					</button>
				</div>
				<button>
					Ingresar
				</button>
			</div>
		</div>
		</NavBarStyled>
	)
}

export default NavBar