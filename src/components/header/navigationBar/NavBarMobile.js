import React from 'react';
import styled from 'styled-components';
import BusinessOutlinedIcon from '@material-ui/icons/BusinessOutlined';
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import PeopleOutlineIcon from '@material-ui/icons/PeopleOutline';
import MailOutlineOutlinedIcon from '@material-ui/icons/MailOutlineOutlined';

const NavBarMobileStyled = styled.div`
	width: 400px;
	height: 100vh;
	position: fixed;
	top: 0;
	right: 0;
	z-index: 99;
	transition: .4s;
	transform: ${({isOpen}) => isOpen ? 'translateX(0px)' : 'translateX(400px)'};
	display: block;
	background: var(--navBarMobile);
	border-radius: 6px;
	ul {
		margin: 0;
		padding: 0;
		height: 100%;
		display: flex;
		justify-content: center;
		align-items: center;
		flex-direction: column;
		row-gap: 1em;
		li {
			width: 100%;
			height: 12%;
			list-style: none;
			display: flex;
			justify-content: center;
			align-items: center;
			border-radius: 10px;
			position: relative;
			overflow: hidden;
			cursor: pointer;
			.icon-box {
				width: 100%;
				height: 100%;
				position: absolute;
				display: flex;
				justify-content: center;
				align-items: center;
				transition: .2s;
				border-radius: 10px;
				cursor: pointer;
				background-image: linear-gradient(15deg, #13587a 0%, #80d0c7 100%);
				opacity: 0;
			}
			&:hover > .icon-box {
				opacity: 1;
				transform: rotateY(20deg);
			}
		}
		a {
			text-decoration: none;
			font-size: 1rem;
			font-weight: 500;
			color: var(--textContrast);
		}
	}
	@media screen and (min-width: 1000px) {
		display: none;
	};
	@media screen and (max-width: 375px) {
		width: 300px;
		transform: ${({isOpen}) => isOpen ? 'translateX(0px)' : 'translateX(300px)'};
	}
`;

function NavBarMobile({ isOpen, handleToggleMenu }) {
	const iconStyles = {
		color: 'white',
		fontSize: 40
	}
	return (
		<NavBarMobileStyled isOpen={isOpen}>
			<ul className="list-enlaces">
				<li>
				<a 
				href="#home" 
				className="icon-box"
				onClick={handleToggleMenu}
				>
				<div>
				<HomeOutlinedIcon style={iconStyles} />
				</div>
				</a>
				<a href="#home">Inicio</a>
				</li>
				<li>
				<a 
				href="#nosotros" 
				className="icon-box"
				onClick={handleToggleMenu}
				>
				<div>
				<InfoOutlinedIcon style={iconStyles} />
				</div>
				</a>
				<a href="#nosotros">Nosotros</a>
				</li>
				<li>
				<a 
				href="#franquicia" 
				className="icon-box"
				onClick={handleToggleMenu}
				>
				<div>
				<BusinessOutlinedIcon style={iconStyles} />
				</div>
				</a>
				<a href="#franquicia">Franquicia</a>
				</li>
				<li>
				<a 
				href="#clients" 
				className="icon-box"
				onClick={handleToggleMenu}
				>
				<div>
				<PeopleOutlineIcon style={iconStyles} />
				</div>
				</a>
				<a href="#clients">Clientes</a>
				</li>
				<li>
				<a 
				href="#contact" 
				className="icon-box"
				onClick={handleToggleMenu}
				>
				<div>
				<MailOutlineOutlinedIcon style={iconStyles} />
				</div>
				</a>
				<a href="#contact">Contactanos</a>
				</li>
			</ul>
		</NavBarMobileStyled>
	)
}

export default NavBarMobile