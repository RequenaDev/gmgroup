import React, { useRef } from 'react';
import styled from 'styled-components';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import homeImage from 'assets/home.png';
import logoLight from 'assets/logo-light.png';

const PresentationStyled = styled.div`
	width: 100%;
	height: 88vh;
	display: flex;
	justify-content: center;
	align-items: center;
	background-image: var(--presentationGradient), url(${homeImage});
	background-position: center;
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: cover;
	position: relative;
	box-sizing: border-box;
	clip-path: polygon(0 0, 100% 0, 100% 86%, 0 100%);
	.presentation-1 {
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	width: 60%;
	height: 100%;
	img {
		margin-top: -10em;
		width: 375px;
		height: 40%;
	}
	}
	.presentation-2 {
		width: 40%;
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
	}
	h1 {
		margin: 0;
		padding: 0;
		font-size: 60px;
		margin-top: -1em;
		background-image: var(--titleGradient);
  		-webkit-background-clip: text;
  		-webkit-text-fill-color: transparent;
  		font-weight: 800;
  		text-align: center;
  		dislay: block;
  		overflow: hidden;
	}
	button {
		font-size: 22px;
		color: var(--white);
		background: var(--contactBtnBg);
		padding: 1em 2em;
		border-radius: 10px;
		margin-top: 4em;
		border-bottom: 2px solid rgba(0,0,0, .44);
		box-sizing: border-box;
		margin-top: -3em;
	}
	p {
		position: absolute;
		top: 0;
		right: 2em;
		display: flex;
		font-size: .9rem;
		justify-content: center;
		align-items: center;
		color: var(--tcReverse);
		font-weight: 600;
		cursor: pointer;
	}
	.letter {
    	transition: .6s;
	}

	.fade {
		background-image: var(--primaryGradient);
  		-webkit-background-clip: text;
  		-webkit-text-fill-color: transparent;
	}
	@media screen and (max-width: 765px) {
		flex-direction: column;
		clip-path: polygon(0 0, 100% 0, 100% 96%, 0 100%);
		button {
			margin-top: -10em;
		}
	}
	@media screen and (max-width: 375px) {
		h1 {
			font-size: 50px;
		}
	}
`;

function Presentation({ handleToggleModal, language }) {
const sloganRef = useRef();

	return (
		<PresentationStyled id="home">
		<div className="presentation-1">
			<img src={logoLight} alt="" />
			<h1 className="slogan" ref={sloganRef}>
			{
				language === 'ES'
				? "SOLUCIONES EN COBRANZAS"
				: "COLLECTION SOLUTIONS"
			}
			</h1>
		</div>
		<div className="presentation-2">
			<p>
			<GroupAddIcon />
			{
				language === 'ES'
				? "TRABAJA CON NOSOTROS"
				: "WORK WITH US"
			}
			</p>
			<button onClick={handleToggleModal}>
				{
					language === 'ES'
					? 'Contactanos'
					: 'Contact us'
				}
			</button>
		</div>
		</PresentationStyled>
	)
}

export default Presentation