import React from 'react';
import styled from 'styled-components';
import NavBar from './navigationBar/NavBar';
import NavBarMobile from './navigationBar/NavBarMobile';
import Presentation from './Presentation';

const HeaderStyled = styled.div`

`;

function Header({
	isOpen,
	handleToggleModal,
	handleToggleMenu,
	language,
	setLanguage,
	darkMode,
	setDarkMode
	 }) {
	return (
		<HeaderStyled id="#home">
		<NavBar
		darkMode={darkMode}
		setDarkMode={setDarkMode}
		language={language}
		setLanguage={setLanguage}
		/>
		<NavBarMobile
		isOpen={isOpen}
		handleToggleMenu={handleToggleMenu}
		/>
		<Presentation
		language={language}
		handleToggleModal={handleToggleModal}
		/>
		</HeaderStyled>
	)
}

export default Header