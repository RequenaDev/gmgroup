import React, { useState, useEffect } from 'react';
import 'index.css';
import styled from 'styled-components';
import Header from 'components/header/Header';
import Empresa from 'components/empresa/Empresa';
import Countries from 'components/countries/Countries';
import ClientMain from 'components/clients/ClientMain';
import Historys from 'components/clients/Historys';
import ConsultModal from 'components/ConsultModal';
import ClientBanner from 'components/clients/ClientBanner';
import Questions from 'components/questions/Questions';
import QuestionsBanner from 'components/questions/QuestionsBanner';
import News from 'components/news/News';
import Footer from 'components/footer/Footer';
import { Sling as Hamburger } from 'hamburger-react';

const AppStyled = styled.div`
  * {
    scroll-behavior: smooth;
  }
  background: var(--bodyBg);
  position: relative;
  .hamburger-box {
      z-index: 100;
      position: fixed;
      bottom: 1em;
      right: 1em;
      display: none;
  }
  @media screen and (max-width: 1000px) {
    .hamburger-box {
      display: flex;
    }
  }
`;

function App() {
  const [language, setLanguage] = useState('ES');
  const [darkMode, setDarkMode] = useState(false);
  const [isOpen, setOpen] = useState(false);
  const [consultModal, setConsultModal] = useState(null);

  const theme = window.localStorage.getItem('appTheme');
  const lan = window.localStorage.getItem('appLan');

  if (!lan) {
    window.localStorage.setItem(
      'appLan', 'ES'
    )
  }

  if(!theme) {
    window.localStorage.setItem(
    'appTheme', 'false'
    );
  }

  const handleToggleMenu = (e) => {
      setTimeout(() => {
        setOpen(!isOpen);
      }, 200);
  };

  const handleToggleModal = (e) => {
    e.stopPropagation();
    setConsultModal(!consultModal);
  };

  useEffect(() => {
    setTimeout(() => {
      setConsultModal(true);
  },3000)
  }, []);

  useEffect(() => {
    if(theme) {
    setDarkMode(
      theme === 'true' ? true : false
    );
    if(lan) {
      setLanguage(
        lan === 'ES' ? 'ES' : 'EN'
      )
    }
  }
  }, [theme, lan])

  return (
    <AppStyled className={darkMode ? 'dark-mode' : 'light-mode'}>
    <Header
    isOpen={isOpen}
    handleToggleModal={handleToggleModal}
    handleToggleMenu={handleToggleMenu}
    language={language}
    setLanguage={setLanguage}
    darkMode={darkMode}
    setDarkMode={setDarkMode}
    />
    <Empresa
    language={language}
    />
    <Countries
    language={language}
    handleToggleModal={handleToggleModal}
    />
    <ClientMain
    language={language}
    />
    <Historys language={language} />
    <ClientBanner
    language={language}
    handleToggleModal={handleToggleModal}
    />
    <Questions
    language={language}
    />
    <QuestionsBanner
    language={language}
    handleToggleModal={handleToggleModal}
    />
    <News
    language={language}
    />
    <Footer
    language={language}
    />
    {
      consultModal
      ? <ConsultModal
        handleToggleModal={handleToggleModal}
        language={language}
      />
      : null
    }
    <div className="hamburger-box">
    <Hamburger
    color={"#1c5e80"}
    size={100}
    rounded
    toggled={isOpen}
    toggle={handleToggleMenu}
    />
    </div>
    </AppStyled>
  );
}

export default App;
